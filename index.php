<?php

require_once 'functions.php';
require_once 'vendor/tpl.php';
require_once 'MySQLFunctions.php';

if (isset($_GET["cmd"]) && $_GET["cmd"] == "show-book-add-submit") {
    if (isset($_POST["deleteButton"])) {
        deleteBook($_POST["id"]);
        header("Location:/?cmd=show-book-list&message=deleted");
        exit();
    } elseif ($_SERVER["REQUEST_METHOD"] == "POST" && isValidTitle($_POST["title"])){
        if (isset($_POST["grade"])) {
            $gr = $_POST["grade"];
        } else {
            $gr = null;
        }
        if (isset($_POST["isRead"])){
            $iR = $_POST["isRead"];
        } else {
            $iR = null;
        }
        if (isset($_POST["id"])){
            updateBook($_POST["id"], $_POST["title"], $gr, $iR, $_POST["author1"], $_POST["author2"]);
            header("Location:/?cmd=show-book-list&message=updated");
        } else {
            insertBook($_POST["title"], $gr, $iR, $_POST["author1"], $_POST["author2"]);
            header("Location:/?cmd=show-book-list&message=saved");
        }
        exit();
    }
} elseif (isset($_GET["cmd"]) && $_GET["cmd"] == "show-author-add-submit") {
    if (isset($_POST["deleteButton"])){
        deleteAuthor($_POST["id"]);
        header("Location:/?cmd=show-author-list&message=deleted");
        exit();
    } elseif ($_SERVER["REQUEST_METHOD"] == "POST" && isValidFirstAndLastName($_POST["firstName"], $_POST["lastName"])) {
        if (isset($_POST["grade"])) {
            $gr = $_POST["grade"];
        } else {
            $gr = null;
        }
        if (isset($_POST["id"])) {
            updateAuthor($_POST["id"], $_POST["firstName"], $_POST["lastName"], $_POST["grade"]);
            header("Location:/?cmd=show-author-list&message=updated");
        } else {
            insertAuthor($_POST["firstName"], $_POST["lastName"], $gr);
            header("Location:/?cmd=show-author-list&message=saved");
        }
        exit();
    }
}

?>
<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="UTF-8">
    <title>Harjutus</title>
    <link href="styles.css" rel="stylesheet">
</head>
<body>
<?php

require ("menu.html");
if ( isset($_GET["cmd"])) {
    if ($_GET["cmd"] == "show-book-list") {
        $books = getBooks();
        if (isset($_GET["message"])){
            $data = ["books" => $books, "message" => $_GET["message"]];
        } else {
            $data = ["books" => $books];
        }
        $contentPath = "book-list.html";
        print renderTemplate('book-list.html', $data);
    } elseif ($_GET["cmd"] == "show-book-add") {
        $authors = getAuthors();
        $data = ["authors" => $authors];
        $contentPath = "book-add.html";
        print renderTemplate('book-add.html', $data);
    } elseif ($_GET["cmd"] == "show-book-add-submit") {
        if ($_SERVER["REQUEST_METHOD"] == "POST" && !isValidTitle($_POST["title"])) {
            $authors = getAuthors();
            $book = getBookByPost();
            if (isset($_POST["id"])){
                $data = ["authors" => $authors, "book" => $book, "db" => true];
            } else {
                $data = ["authors" => $authors, "book" => $book];
            }
            $contentPath = "book-add-submit.html";
            print renderTemplate('book-add-submit.html', $data);
        }
    } elseif ($_GET["cmd"] == "show-book-edit") {
        $bookId = $_GET["id"];
        $authors = getAuthors();
        $book = getBookById($bookId);
        $data = ["authors" => $authors, "book" => $book];
        $contentPath = "book-edit.html";
        print renderTemplate('book-edit.html', $data);
    } elseif ($_GET["cmd"] == "show-author-list") {
        $authors= getAuthors();
        if (isset($_GET["message"])){
            $data = ["authors" => $authors, "message" => $_GET["message"]];
        } else {
            $data = ["authors" => $authors];
        }
        $contentPath = "author-list.html";
        print renderTemplate('author-list.html', $data);
    } elseif ($_GET["cmd"] == "show-author-add") {
        $contentPath = "author-add.html";
        print renderTemplate('author-add.html');
    } elseif ($_GET["cmd"] == "show-author-add-submit") {
        if ($_SERVER["REQUEST_METHOD"] == "POST" && !isValidFirstAndLastName($_POST["firstName"], $_POST["lastName"])){
            $author = getAuthorByPost();
            if (isset($_POST["id"])) {
                $data = ["author" => $author, "db" => true];
            } else {
                $data = ["author" => $author];
            }
            $contentPath = "author-add-submit.html";
            print renderTemplate('author-add-submit.html', $data);
        }
    } elseif ($_GET["cmd"] == "show-author-edit") {
        $authorId = $_GET["id"];
        $author = getAuthorById($authorId);
        $data = ["author" => $author];
        $contentPath = "author-edit.html";
        print renderTemplate("author-edit.html", $data);
    }
} else {
    $books = getBooks();
    if (isset($_GET["message"])){
        $data = ["books" => $books, "message" => $_GET["message"]];
    } else {
        $data = ["books" => $books];
    }
    $contentPath = "book-list.html";
    print renderTemplate('book-list.html', $data);
}

?>
<footer>Harjutus</footer>
</body>
</html>
