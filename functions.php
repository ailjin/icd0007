<?php
require_once ('MySQLFunctions.php');

function getBooks(){

    $books = [];

    $prevId = null;
    foreach (request("
    SELECT books.id, title, books.grade, a.id, a.first_name, a.last_name, a2.first_name, a2.last_name
    FROM books
    LEFT JOIN author_book ab on books.id = ab.book_id
    LEFT JOIN authors a on ab.author_id = a.id
    LEFT JOIN author_book ab2 on books.id = ab2.book_id and ab2.id != ab.id
    LEFT JOIN authors a2 on a2.id = ab2.author_id and a2.id != a.id
    ORDER BY books.id, ab.id", null, null) as $book) {
        if ($prevId !=  $book[0]){
            $books[] = $book;
        }
        $prevId = $book[0];
    }
    return $books;
}

function getAuthors(){
    $authors = [];
    foreach (request("SELECT * FROM authors", null,null) as $author){
        $authors[] = $author;
    }
    return $authors;
}

function isValidTitle($title){
    if (strlen($title) > 2 && strlen($title) < 24){
        return true;
    } else {
        return false;
    }
}

function insertBook($title, $grade, $isRead, $author1, $author2){
    $connection = new PDO(ADDRESS, USERNAME, PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

    $stmt = $connection->prepare("INSERT INTO books (title, grade, isRead) VALUES (:newT, :newG, :newIr)");
    $stmt->bindValue(':newT', $title);
    $stmt->bindValue(':newG', $grade);
    $stmt->bindValue(':newIr', $isRead);
    $stmt->execute();
    foreach (request("SELECT * FROM books ORDER BY id DESC LIMIT 1", null, null) as $book){
        $bookID = $book['id'];
        if (trim($author1) != ""){
            request("INSERT INTO author_book (author_id, book_id) VALUES ($author1, $bookID)", null, null);
        }
        if (trim($_POST['author2']) != ""){
            request("INSERT INTO author_book (author_id, book_id) VALUES ($author2, $bookID)", null, null);
        }
    }
}

function getBookByPost(){
    if (isset($_POST["grade"]) && isset($_POST["isRead"])) {
        $book = [1 => $_POST["title"], 2 => $_POST["grade"], 3 => $_POST["isRead"],
            4 => $_POST["author1"], 5 => $_POST["author2"]];
    } elseif (isset($_POST["grade"]) && !isset($_POST["isRead"])) {
        $book = [1 => $_POST["title"], 2 => $_POST["grade"],
            4 => $_POST["author1"], 5 => $_POST["author2"]];
    } elseif (!isset($_POST["grade"]) && isset($_POST["isRead"])) {
        $book = [1 => $_POST["title"],3 => $_POST["isRead"],
            4 => $_POST["author1"], 5 => $_POST["author2"]];
    } else {
        $book = [1 => $_POST["title"], 4 => $_POST["author1"], 5 => $_POST["author2"]];
    }
    return $book;
}

function getBookById($id){
    $prevId = null;
    foreach (request("SELECT books.id, title, books.grade, books.isRead, a.id, a2.id
FROM books
    LEFT JOIN author_book ab on books.id = ab.book_id
    LEFT JOIN authors a on ab.author_id = a.id
    LEFT JOIN author_book ab2 on books.id = ab2.book_id and ab2.id != ab.id
    LEFT JOIN authors a2 on a2.id = ab2.author_id and a2.id != a.id
WHERE books.id = :id
        ORDER BY books.id, ab.id;", ":id", 'id') as $book) {
        if ($prevId !=  $book[0]){
            $book = [0 => $book[0], 1 => $book[1], 2 => $book[2], 3 => $book[3], 4 => $book[4], 5 => $book[5]];
            return $book;
        }
        $prevId = $book[0];
    }
}

function deleteBook($id){
    request(
        "delete from author_book where book_id = $id"
        , null, null);
    request(
        "delete from books where id = $id"
        , null, null);
}

function updateBook($id, $title, $grade, $isRead, $author1, $author2){
    request(
        "delete from author_book where book_id = '$id'"
        , null, null);
    request(
        "UPDATE books SET title = '$title', grade = '$grade', isRead = '$isRead' Where id = '$id'"
        , null, null);
    if ($author1 != "" && $author1 != null){
        request(
            "INSERT INTO author_book (author_id, book_id) VALUES ('$author1', $id)"
            , null, null);
    }
    if ($author2 != "" && $author2 != null){
        request(
            "INSERT INTO author_book (author_id, book_id) VALUES ('$author2', $id)"
            , null, null);
    }
}

function isValidFirstAndLastName ($firstName, $lastName){
    if (strlen($firstName) > 0 && strlen($firstName) < 22 && strlen($lastName) > 1 && strlen($lastName) < 23) {
        return true;
    } else {
        return false;
    }
}

function insertAuthor($firstName, $lastName, $grade){
    $connection = new PDO(ADDRESS, USERNAME, PASSWORD,
        [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);

    $stmt = $connection->prepare("INSERT INTO authors (first_name, last_name, grade) VALUES (:newFn, :newLn, :newG)");
    $stmt->bindValue(':newFn', $firstName);
    $stmt->bindValue(':newLn', $lastName);
    $stmt->bindValue(':newG', $grade);
    $stmt->execute();
}

function getAuthorByPost(){
    if (isset($_POST["grade"])){
        $author = [1 => $_POST["firstName"], 2 => $_POST["lastName"], 3 => $_POST["grade"]];
    } else {
        $author = [1 => $_POST["firstName"], 2 => $_POST["lastName"]];
    }
    return $author;
}

function getAuthorById($id){
    foreach (request("SELECT id, first_name, last_name, grade FROM authors WHERE id = :id", ":id", 'id')
             as $a){
        return [0 => $a[0], 1 => $a[1], 2 => $a[2], 3 => $a[3]];
    }

}

function deleteAuthor($id){
    request(
        "delete from authors where id = $id"
        , null, null);
}

function updateAuthor($id, $firstName, $lastName, $grade){
    request(
        "UPDATE authors SET first_name = '$firstName', last_name = '$lastName', grade = '$grade'Where id = $id"
        , null, null);
}
